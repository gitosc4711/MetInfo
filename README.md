# 米拓企业建站系统（MetInfo）


最新版本：V7.0.0beta（发布于2019年8月3日）

开发语言：PHP+Mysql

环境要求：Nginx、Apache、IIS,PHP5.3-7.2、MYSQL5.0-5.7

支持语言：用户自定义(支持全球各种语言，每套系统可设置无限多种语言)

界面风格：支持米拓建站数百套商业模板及用户自定义模板

版权所有：长沙米拓信息技术有限公司

#### 下载地址：[https://www.metinfo.cn/download/](http://)

#### 官方网站：[https://www.metinfo.cn/](http://)

#### 演示地址：[http://demo.metinfo.cn/](http://)

适用范围：企业官网、个人网站、政府单位网站、学校网站、B2B/B2C商城网站等

发展历程：自2009年3月28日V1.0版本发布至今，全球已有超过50万中小企业网站使用MetInfo


米拓企业建站系统主要用于搭建企业网站，采用PHP+Mysql架构，全站内置了SEO搜索引擎优化机制，支持用户自定义界面语言(全球各种语言)，支持可视化操作、拥有企业网站常用的模块功能（企业简介模块、新闻模块、产品模块、下载模块、图片模块、招聘模块、在线留言、反馈系统、在线交流、友情链接、网站地图、会员与权限管理、TAG标签）。收费应用插件已上线支付接口、在线商城、系统安全修复、图片加速、网站广告插件等功能。强大灵活的后台管理功能、伪静态及静态页面生成功能、个性化模块添加功能、不同栏目自定义轮播大图样式功能等可为企业打造出大气漂亮且具有营销力的精品响应式网站。


## MetInfo功能介绍

#### 界面风格

1、响应式布局，自适应电脑、手机、平板等访问终端；

2、支持前台网站风格模板切换，支持米拓建站官方商业模板和用户自定义模板；

3、用户可设置网站模板总体风格，如整体色调、背景、字体、文字颜色等；

4、多语言网站可以使用同一套模板，也可以为每种语言设置不同的模板；

5、支持自定义模板，懂WEB前端技术即可自行编写或修改模板，无需专业的PHP程序员；

6、支持缩略图自动生成功能和图片水印功能；

7、内置banner动态大图展示功能（一般位于导航下方），可以为每个栏目设置不同展示图片；

8、产品详情页支持多图展示，可以点击放大查看图片细节；

9、可以设置每页信息的显示条数、显示的时间格式、列表页的展示方式；

10、模板源码编写细致并经过严谨的测试，能够完美兼容IE9+、Firefox、Chrome、Safari、Opera、UC、华为手机浏览器等主流浏览器；

11、支持在线一键安装官方商业模板和演示数据，支持在线升级商业模板；

12、支持自定义banner按钮、文字、颜色等；

13、支持自定义栏目和信息标题文字大小和颜色；

14、支持按栏目自定义缩略图尺寸大小；


#### 移动端

1、响应式布局，和web端同一套模板，支持Android/IOS/Win8；

2、无需重新添加内容，直接调用电脑网站内容；

3、支持与微信公众号完美结合实现微官网；

4、支持单独设置手机网站LOGO\banner图及统计代码；

5、商业在线客服应用支持一键拨打功能，支持设置QQ客服手机在线交流功能；

6、商城模块支持微商城；

7、支持手机端管理网站；

8、支持自定义手机底部菜单；


#### 可视化编辑

1、傻瓜式操作，哪里不会点哪里；

2、支持图片、文字一键修改；

3、支持区块颜色、文字颜色等风格在线设置；

4、支持区块开启与隐藏设置；

5、支持可视化添加编辑网站内容、栏目及参数设置等；


#### 网站内容

1、支持简介、文章、产品、下载、图片、招聘等信息的发布；

2、简介、文章、产品、下载、图片模块支持三级栏目（分类），同级栏目（分类）不限数量；

3、内容编辑框采用UEditor编辑器，可以实现如word文字排版软件般强大的内容排版效果，支持内容分页、视频上传、下载图片到本地；

4、支持内容的删除、移动、复制、排序、推荐、置顶等操作；

5、支持定时发布信息，支持设置信息外链（链接到自定义网址）；

6、产品、图片、下载支持自定义参数的功能，如产品的价格、品牌、附件、多张产品展示图片等；

7、文章、产品、下载、图片内容模块支持回收站功能，如果误删可以在回收站找到并恢复；

8、支持全站搜索，通过模板配合可以按栏目、模块、标题、全文进行搜索；

9、支持产品模块内容页选项卡功能，支持按栏目设置选项卡个数与名称；


#### SEO优化

1、可以自定义全站及每个页面的标题(title)、关键词(keywords)、页面描述(description)；

2、支持静态页面生成功能，可设置自动生成或手动生成，支持自定义每个页面的静态页面名称；

3、支持伪静态功能，可自定义伪静态页面名称(URL)；

4、支持站内锚文本、TAG、上一条下一条功能，用于增加网站内链和突出关键词；

5、前台模板源码采用CSS3+HTML5标准框架，语义化标签更容易让搜索引擎读懂；

6、前台模板合理使用meta标签、h1\h2\h3标签、图片ALT、超链接Title，让搜索引擎更容易读懂页面；

7、前台模块CSS、HTML、Javascript完全分离，结构层次清晰能够增加搜索引擎的友好度；

8、前台源码精简，很受搜索引擎的青睐；

9、支持网站地图功能，用于引导搜索引擎抓取网站页面，能够生成HTML、xml、txt三种格式；

10、支持友情链接功能，可设置首页及各个栏目独立的友情链接数据；

11、强大完善的TAG标签功能，可手动添加管理tag标签，设置每个tag标签的tdk和聚会范围，支持页面内容按tag标签自动聚合；

12、支持sitemap、robots文件自动生成；

13、支持自定义404页面内容；

14、支持不带www域名自动301跳转到带www域名功能；


#### 互动营销

1、内置在线交流功能、可添加QQ、MSN、阿里旺旺、SKYPE、第三方网页客服软件、微信二维码等；

2、内置在线反馈系统，用于添加反馈表单，支持自定义表单内容，可用于在线询单、产品订购、在线报名、在线调查、意见反馈等，访客提交表单后可设置自动发送一封邮件到设定的邮箱或自动发送短信；

3、支持在线留言功能，管理员可以通过后台审核和回复留言内容；

4、可添加第三方统计代码，能够记录来访信息、受访页面信息、来路信息、搜索引擎信息，用户可以对访问信息进行分析后规划设计推广方向；

5、内置短信功能，支持批量发送，可以设置短信提醒服务（如访客提交订购\报名\反馈等表单等）；


#### 支持语言

1、采用 UTF-8 编码，兼容全球所有的语言；

2、用户可自定义网站语言且不限制语言数量，用户需翻译网站内容后录入到对应语言；

3、支持设置网站默认语言（访问网站时默认展示的语言）；

4、支持中繁体切换功能（采用Javascript脚本替换页面中繁体文字，不需要自行翻译）；

5、支持不同语言采用不同域名访问；

6、前台页面设置便于切换网站语言的文字链接或国旗图片链接功能；

7、网站后台管理支持多语言，可以在登录界面选择即将操作的后台界面语言（自带中英文默认语言）；


#### 会员功能

1、支持手机、邮箱等多种在线注册方式；

2、支持自定义会员组，可以设置各个会员组的权限值，权限值影响页面的阅读权限；

3、支持为每个栏目和页面设置阅读权限，只有达到权限要求的用户才能够访问该页面；

4、支持设置模块参数(如产品价格)只能会员或指定会员组的会员查看，或下载资料只能会员或指定会员组下载；

5、商城模块支持按会员组设置不同的价格折扣和优惠；

6、配合支付接口可设置付费会员功能；


#### 后台管理员功能

1、安装时系统设置最高管理权限的管理员，该管理员无法被删除或修改权限；

2、支持新增、删除、编辑管理员，可以设置管理员拥有后台哪些功能的操作权限，或者限制其删除、修改、新增功能；

3、可以限制某个管理员只能够管理一种或多种网站语言；

4、可以设置管理员是否拥有在线更新程序的权限等；


#### 安全与效率

1、支持网站数据备份，可以一键备份整站并下载到本地电脑，可以在后台恢复备份的数据；

2、后台登录页面的路径名称可以修改，用于隐藏后台登录网址，提高网站安全性能；

3、内置的验证码、防刷新机制、SQL危险符号和语句过滤机制能够有效阻拦恶意攻击；

4、PHP+MYSQL架构，能够跨平台运行，适合放在Linux平台，更安全更高效；

5、支持在线升级；

6、前台页面采用缓存机制，有效的提升访问速度，降低服务器资源消耗；

7、全站采用相对路径，可轻松从http切换至https；

8、支持服务器环境检测功能，用户可自行在后台测试是否支持全部功能；

9、支持后台关键操作记录日志功能；

10、支持管理员密码找回功能，可通过手机短信、邮件找回管理员密码；


#### 源码与二次开发

1、开源版本100%开源，可轻松进行二次开发；

2、后台可关闭调用系统默认css、js功能，自定义制作模板方便灵活；


## MetInfo主要优势

核心优势：历经10年打磨，拥有超50万中小企业用户，系统功能完善、安全稳定、简单易用！


#### 一、全站内置SEO优化机制，使企业网站更容易被推广、更具营销力

全站运用了完整的SEO技术，包括全站静态页面自动生成、自定义静态页面名称、伪静态功能、SEO参数后台设置、目录式URL、自定义每个页面的title或全局设置title结构、DIV+CSS构架（H1、Alt、Title、页面结构优化等）、TAG聚合标签、友情链接、热门标签、网站地图(xml|txt|html)、robots.txt文件等。

SEO使网站更容易推广、更容易被搜索引擎收录、更容易提升关键词排名，从而更容易为企业带来客户，MetInfo始终认为，只有具有营销力的企业网站才是真正具有价值的网站。


#### 二、操作简单方便，可视化编辑，只要会打字便可以管理更新全部网站内容

区别与大多数系统以技术和功能实现为系统开发核心思路，米拓企业建站系统完全以用户体验效果和操作习惯为核心开发思路。

V6版本开始支持可视化操作，可以轻松修改网站文字、图片等全部内容网站，便捷的多语言切换配置功能，任何企业网站管理员，无需专业技术知识，便可以轻松管理网站。


#### 三、支持网站语言自定义，可轻松添加简体中文、繁体中文、英文、日文、韩文或自定义的其他任意语言

独特的语言包后台修改编辑方式，管理员只需要登录后台修改语言包和设置语言名称便可以轻松打造出各种语言的企业网站。


#### 四、响应式网站风格，电脑、平板、手机端同样美好！

采用bootstarp响应式布局，一套代码可以完美自适应各种访问终端。

拥有独立的手机端管理后台，手机也能管理网站。


#### 五、支持多种界面风格及展现形式、个性化模板制作简单灵活，让您的网站漂亮大气、个性化实足！

用户可以在后台轻松切换网站前台模板、设置模板颜色风格等；

全站文字、系统栏目、信息内容及主要图片支持用户自定义，所见即可改。


#### 六、系统功能强大灵活，可为您打造功能完善的个性化营销型企业网站

内置完整的企业网站功能模块，包括：简介、文章、产品、下载、图片、招聘、会员、搜索、留言、反馈、友情链接、网站地图、TAG标签等模块；

支持三级栏目任意添加，可以控制栏目显示状况、设置栏目文件夹、栏目排序、栏目中信息排序方式等等；

内置批量上传功能、批量缩略图功能、批量打水印功能，有效减少了网站维护工作量。


#### 七、内置强大的交互式营销工具，让您的网站不只是一个死板的摆设

内置在线客服、在线反馈、在线留言等功能，并可以设置自动发送邮件和短信；

内置第三方统计代码及其他工具代码接口，可以轻松监控网站浏览及来访者详情。


#### 八、安全稳定，快速高效，内置强大的权限管理和安全机制

PHP+MYSQL构架，支持多平台，安全高效，使您的网站远离木马和恶意攻击的侵扰；

数据库后台备份和一键恢复功能，外部提交防注入功能，使网站安全稳定且易于转移；

验证码、防刷新机制、SQL过滤机制、后台管理文件夹名称更改、管理员密码找回、上传文件管理、安装升级文件自动删除等让您的网站运行安全无忧；

特有的管理员权限管理机制，可以灵活设置管理员的栏目管理权限、网站信息的添加、修改、删除权限等。


#### 九、代码开源，完全拥有自主知识产权，可在线升级，易于二次开发

代码开源（商业模板机制编译及判断机制除外）、结构清晰、简单易读，二次开发及模板制作简单方便；

完全拥有自主知识产权（版权登记号：2009SR039973，2013SR051827，2018SR150893），系统采用免费开源的PHP语言、MYSQL数据库，让您远离版权纠纷。


